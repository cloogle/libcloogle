# Changelog

#### v2.2.1

- Chore: accept base 3.x, containers 2.x, json 3.x, and text 2.x.

### v2.2.0

- Chore: update to base 2.0.

### v2.1.0

- Enhancement: add `ServerError` constructor to `CloogleError` (response code
  131).

## v2.0.0

- Change: `ProblemResult`s now have basic information as well (mostly for
  `distance`).
- Documentation change: spaces are now allowed in `name` requests.

#### v1.2.1

- Chore: move to https://clean-lang.org

### v1.2

- Change: added `problem_authors` field to `ProblemResult`.

### v1.1

- Enhancement: added optional `version` field to `BasicResult`.
- Change: added version element to `LocationResult`.

## v1.0

First tagged version.
