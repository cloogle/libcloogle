# CONTRIBUTING

This repository describes the Cloogle API as a set of types. Because many
applications make assumptions about this API, one must be reluctant with
changing this.

This is a non-exhaustive list of projects that depend on the API and could be
checked when making changes.

- [vim-clean][]
- [cloogle.py][]

Inactive projects:

- [cloogle-irc][]
- [cloogle-mail][]
- [CloogleBot][]
- [cloogle-cli][]
- [cloogle-discord][]
- [CleanForVSCode][]

[CloogleBot]: https://gitlab.com/cloogle/archived/clooglebot
[cloogle-cli]: https://gitlab.com/cloogle/archived/cloogle-cli
[cloogle-discord]: https://github.com/ErinvanderVeen/cloogle-discord
[cloogle-irc]: https://git.martlubbers.net/?p=cloogle-irc.git;a=summary
[cloogle-mail]: https://gitlab.com/cloogle/archived/cloogle-mail
[cloogle.py]: https://gitlab.com/cloogle/periphery/cloogle.py
[vim-clean]: https://gitlab.com/clean-and-itasks/vim-clean
[CleanForVSCode]: https://github.com/W95Psp/CleanForVSCode
