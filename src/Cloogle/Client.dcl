definition module Cloogle.Client

/**
 * Functions to access the HTTP frontend of Cloogle.
 *
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of libcloogle.
 *
 * Libcloogle is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Libcloogle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with libcloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError, :: MaybeErrorString
from Internet.HTTP import :: HTTPRequest

import Cloogle.API

/**
 * Create a `HTTPRequest` from a Cloogle `Request`.
 * This function does not actually perform the request; see `request`.
 * @param The server to use, e.g. `example.org`.
 * @param The Cloogle `Request`.
 * @result The corresponding `HTTPRequest`.
 */
toHTTPRequest :: !String !Request -> HTTPRequest

/**
 * Perform a Cloogle `Request` and return a `Response`.
 * @param The server to connect to, e.g. `example.org`.
 * @param The Cloogle `Request`.
 * @result The `Response`, or an error.
 */
request :: !String !Request !*World -> *(MaybeErrorString Response, *World)
