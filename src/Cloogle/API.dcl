definition module Cloogle.API

/**
 * The API for Cloogle, a search engine for Clean.
 *
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of libcloogle.
 *
 * Libcloogle is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Libcloogle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with libcloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError, :: MaybeErrorString
from Data.GenEq import generic gEq
from StdOverloaded import class zero, class fromString, class toString,
	class fromInt, class toInt, class <, class ==
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

/**
 * A Cloogle request.
 */
:: Request =
	{ unify            :: ! ?String   //* Functions that unify with this type
	, name             :: ! ?String   //* Entries that match this name
	, exactName        :: ! ?String   //* Exactly this name
	, className        :: ! ?String   //* Exactly this name, and a class
	, typeName         :: ! ?String   //* Exactly this name, and a type
	, using            :: ! ?[String] //* Return everything that uses all of these exact names (types or classes)
	, modules          :: ! ?[String] //* Modules to search in
	, libraries        :: ! ?[String] //* Libraries to search in
	, include_builtins :: ! ?Bool     //* Whether language builtins should be included
	, include_core     :: ! ?Bool     //* Whether library core modules should be included
	, page             :: ! ?Int      //* Pagination of the results
	}

/**
 * A Cloogle response.
 */
:: Response =
	{ return         :: !Int        //* A return code (use {{`fromInt`}} of {{`CloogleError`}})
	, data           :: ![Result]   //* The actual results
	, msg            :: !String     //* A human-readable message describing the return code
	, more_available :: ! ?Int      //* How many more results are available in next pages
	, suggestions    :: ! ?[(Request, Int)]
	                                //* Suggestions for related requests and how many results they have
	, warnings       :: ! ?[String] //* Human-readable warnings
	}

/**
 * A single Cloogle result.
 */
:: Result
	= FunctionResult FunctionResult //* A normal function, macro, generic, class member, constructor or record field
	| TypeResult TypeResult         //* A type definition
	| ClassResult ClassResult       //* A class definition
	| ModuleResult ModuleResult     //* A module
	| SyntaxResult SyntaxResult     //* A syntax element
	| ABCInstructionResult ABCInstructionResult //* An ABC machine instruction
	| ProblemResult ProblemResult   //* A common problem (see https://gitlab.com/cloogle/common-problems)

/**
 * Basic data that is included in any Cloogle result.
 */
:: BasicResult =
	{ library       :: !String   //* The library the entry was found in
	, version       :: !?String  //* The version of the library
	, license       :: !?LicenseDetails //* Details of the license the library is licensed under
	, filename      :: !String   //* The basename of the definition module
	, modul         :: !String   //* The full (hierarchical) module name
	, dcl_line      :: ! ?Int    //* The line in the definition module of this entry
	, icl_line      :: ! ?Int    //* The line in the implementation module of this entry
	, name          :: !String   //* The name of the entry
	, distance      :: ! ?Real   //* A distancy measure, lower is more relevant
	, builtin       :: ! ?Bool   //* Whether this is a builtin function
	, documentation :: ! ?String //* The CleanDoc description for this result
	, langrep_documentation :: ! ?[CleanLangReportLocation] //* Documentation in the Clean language report
	}

/**
 * Details about a license.
 */
:: LicenseDetails =
	{ id   :: !String //* The SPDX identifier
	, name :: !String //* The full name of the license
	}

/**
 * A location in a Clean language report.
 */
:: CleanLangReportLocation =
	{ clr_version :: String //* The version of the report
	, clr_section :: String //* The section in the report
	}

/**
 * Cloogle result about a function, generic, class member, constructor or
 * record field.
 *
 * @representation A tuple of basic and function-specific data
 */
:: FunctionResult :== (BasicResult, FunctionResultExtras)

/**
 * Different kinds of functions.
 */
:: FunctionKind
	= Function    //* A normal function or generic
	| Macro       //* A macro
	| Constructor //* An ADT constructor
	| RecordField //* A record field
	| ClassMember //* A class member
	| ClassMacro  //* A class macro

/**
 * Function-specific Cloogle result data.
 */
:: FunctionResultExtras =
	{ func                :: !String       //* A string representation of the type
	, kind                :: !FunctionKind //* The kind of function
	, unifier             :: ! ?StrUnifier //* A unifier, if the request included a type
	, required_context    :: ! ?[(String, [LocationResult])] //* Instances or derivations that are needed (if the request included a type)
	, cls                 :: ! ?ShortClassResult //* The class, if this is a class member
	, constructor_of      :: ! ?String     //* The type that this is a constructor of
	, recordfield_of      :: ! ?String     //* The type that this is a record field of
	, generic_derivations :: ! ?[(String, [LocationResult])]
	    //* Derivations, if this is a generic function, and their locations
	, param_doc           :: ! ?[String]   //* Documentation of the parameters
	, generic_var_doc     :: ! ?[String]   //* Documentation of the generic type variables
	, result_doc          :: ! ?[String]   //* Documentation of the result(s)
	, type_doc            :: ! ?String     //* Documentation of the type (for macros)
	, throws_doc          :: ! ?[String]   //* Documentation of the exceptions
	}

/**
 * Cloogle result about a type definition.
 *
 * @representation A tuple of basic and type-specific data
 */
:: TypeResult :== (BasicResult, TypeResultExtras)

/**
 * Type-specific Cloogle result data.
 */
:: TypeResultExtras =
	{ type                    :: !String
	  //* A string representation of the definition
	, type_instances          :: ![(String, [String], [LocationResult])]
	  //* Instances of the type: the class, the class parameters and the locations of the instance
	, type_derivations        :: ![(String, [LocationResult])]
	  //* Generic derivations of the type: the generic and the locations of the derivation
	, type_var_doc            :: !?[String]
	  //* Documentation of the type variables
	, type_field_doc          :: !?[?String]
	  //* Documentation of the record fields
	, type_constructor_doc    :: !?[?String]
	  //* Documentation of the constructors
	, type_representation_doc :: !?String
	  //* Documentation of the synonym type representation
	}

/**
 * Cloogle result about a class definition
 *
 * @representation A tuple of basic and class-specific data
 */
:: ClassResult :== (BasicResult, ClassResultExtras)

/**
 * Class-specific Cloogle result data.
 */
:: ClassResultExtras =
	{ class_name        :: !String      //* The name of the class
	, class_heading     :: !String      //* The class name and variables
	, class_funs        :: ![String]    //* The class members (may contain newlines if there are macros)
	, class_fun_doc     :: ! ?[?String] //* Documentation of the members
	, class_instances   :: ![([String], [LocationResult])]
	  //* Instances of the class: the types and the locations of the instances
	}

/**
 * Cloogle result about a module.
 *
 * @representation A tuple of basic and module-specific data
 */
:: ModuleResult :== (BasicResult, ModuleResultExtras)

/**
 * Module-specific Cloogle result data.
 */
:: ModuleResultExtras =
	{ module_is_core :: Bool //* Whether this module is part of a library core
	}

/**
 * Cloogle result about a language construct.
 *
 * @representation A tuple of basic and construct-specific data
 */
:: SyntaxResult :== (BasicResult, SyntaxResultExtras)

/**
 * Syntax contruct-specific Cloogle result data.
 */
:: SyntaxResultExtras =
	{ syntax_title        :: String //* The name of the construct
	, syntax_code         :: [String] //* Strings describing the construct, as short as possible
	, syntax_examples     :: [SyntaxExample] //* Some code examples (should include comments)
	}

/**
 * An example of syntax.
 */
:: SyntaxExample =
	{ example       :: !String         //* The actual code
	, cleanjs_start :: ! ?String       //* The state in which clean.js should start highlighting
	, bootstrap     :: ![String]       //* Imports and other bootstrapping code required for this example
	, requires_itask_compiler :: !Bool //* Whether this example requires the iTask compiler
	}

/**
 * Cloogle result about an ABC instruction.
 *
 * @representation A tuple of basic and instruction-specific data.
 */
:: ABCInstructionResult :== (BasicResult, ABCInstructionResultExtras)

/**
 * Information about an ABC instruction.
 */
:: ABCInstructionResultExtras =
	{ abc_instruction :: !String        //* The name of the instruction
	, abc_arguments   :: ![ABCArgument] //* The arguments
	}

/**
 * An argument for an ABC instruction.
 */
:: ABCArgument = ABCArgument ABCArgumentType Bool //* The type and whether it is optional or not.

:: ABCArgumentType
	= ABCTypeLabel
	| ABCTypeAStackOffset
	| ABCTypeBStackOffset
	| ABCTypeAStackSize
	| ABCTypeBStackSize
	| ABCTypeBool
	| ABCTypeChar
	| ABCTypeInt
	| ABCTypeReal
	| ABCTypeString

instance toString ABCArgument, ABCArgumentType
instance fromString ABCArgument, ABCArgumentType

/**
 * Cloogle result about a common problem (see https://gitlab.com/cloogle/common-problems).
 *
 * @representation A tuple of basic and problem-specific data.
 */
:: ProblemResult :== (BasicResult, ProblemResultExtras)

:: ProblemResultExtras =
	{ problem_key         :: !String   //* The key (filename in the common-problems repo without extension)
	, problem_title       :: !String   //* The problem title
	, problem_authors     :: !String   //* People with copyright on the text
	, problem_description :: !String   //* A concise description
	, problem_solutions   :: ![String] //* Possible solutions
	, problem_examples    :: ![String] //* A number of examples
	}

/**
 * A location in a Clean library.
 *
 * @representation The library, the version, the module, the filename, the line
 *   in the definition module and the line in the implementation module.
 */
:: LocationResult :== (String, String, String, String, ?Int, ?Int)

/**
 * A type unifier, represented with strings.
 */
:: StrUnifier =
	{ left_to_right :: ![(String,String)] //* Transformations from request to response
	, right_to_left :: ![(String,String)] //* Transformations from response to request
	, used_synonyms :: ![(String,String)] //* Synonyms that were used during unification
	}

/**
 * Basic data about a class definition.
 */
:: ShortClassResult =
	{ cls_name :: !String   //* The name
	, cls_vars :: ![String] //* The type variables
	}

/**
 * A Cloogle Error.
 */
:: CloogleError
	/* Cloogle system errors */
	= NoResults      //* No matching entries were found
	| InvalidInput   //* The input is invalid (e.g., combining typeName and className)
	| InvalidName    //* The name is invalid (e.g., it is too long)
	| InvalidType    //* A type could not be parsed (e.g., in unify)
	| ServerError    //* An unexpected error occurred on the server
	/* Cloogle web frontend errors */
	| ServerDown     //* The Cloogle backend could not be reached
	| IllegalMethod  //* Incorrect HTTP method to the API
	| IllegalRequest //* Incorrect parameters to the API
	| ServerTimeout  //* Connection to the Cloogle backend timed out
	| DosProtection
		//* Too many requests from this client, try again later (this response
		//* is not given any more.)
	| QueryTooLong   //* The query was too long
	/* Wildcard to make fromInt total */
	| OtherCloogleError String //* Another Cloogle error

instance toInt CloogleError
instance fromInt CloogleError

derive JSONEncode Request, Response, Result, ShortClassResult, BasicResult,
	FunctionResultExtras, TypeResultExtras, ClassResultExtras, FunctionKind,
	ModuleResultExtras, SyntaxResultExtras, SyntaxExample,
	ABCInstructionResultExtras, ABCArgument, CleanLangReportLocation,
	StrUnifier
derive JSONDecode Request, Response, Result, ShortClassResult, BasicResult,
	FunctionResultExtras, TypeResultExtras, ClassResultExtras, FunctionKind,
	ModuleResultExtras, SyntaxResultExtras, SyntaxExample,
	ABCInstructionResultExtras, ABCArgument, CleanLangReportLocation,
	StrUnifier
derive gEq FunctionKind

instance zero Request
instance zero Response

instance toString Request
instance toString Response

instance fromString (?Request)

instance == FunctionKind

getBasicResult :: !Result -> BasicResult

toSingleLine :: !Request -> ?String
parseSingleLineRequest :: !String -> MaybeErrorString Request
