implementation module Cloogle.API

/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of libcloogle.
 *
 * Libcloogle is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Libcloogle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with libcloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
from Data.Func import $
import Data.Functor
import Data.GenEq
import Data.Maybe
import StdBool
import StdChar
from StdFunc import flip
import StdInt
import StdMisc
import StdOverloaded
import StdReal
import StdString
import _SystemArray
from Text import class Text(join,split,trim), instance Text String,
	instance + String, <+
import Text.GenJSON

derive JSONEncode ABCInstructionResultExtras, BasicResult, ClassResultExtras,
	CleanLangReportLocation, FunctionKind, FunctionResultExtras,
	LicenseDetails, ModuleResultExtras, ProblemResultExtras, Request, Response,
	Result, ShortClassResult, StrUnifier, SyntaxExample, SyntaxResultExtras,
	TypeResultExtras
derive JSONDecode ABCInstructionResultExtras, BasicResult, ClassResultExtras,
	CleanLangReportLocation, FunctionKind, FunctionResultExtras,
	LicenseDetails, ModuleResultExtras, ProblemResultExtras, Request, Response,
	Result, ShortClassResult, StrUnifier, SyntaxExample, SyntaxResultExtras,
	TypeResultExtras
derive gEq FunctionKind

JSONEncode{|ABCArgument|} _ arg = [JSONString (toString arg)]

JSONDecode{|ABCArgument|} _ [JSONString s:r] = (?Just (fromString s), r)
JSONDecode{|ABCArgument|} _ json = (?None, json)

instance toInt CloogleError
where
	toInt NoResults             = 127
	toInt InvalidInput          = 128
	toInt InvalidName           = 129
	toInt InvalidType           = 130
	toInt ServerError           = 131
	toInt ServerDown            = 150
	toInt IllegalMethod         = 151
	toInt IllegalRequest        = 152
	toInt ServerTimeout         = 153
	toInt DosProtection         = 154
	toInt QueryTooLong          = 155
	toInt (OtherCloogleError _) = -1

instance fromInt CloogleError
where
	fromInt 127 = NoResults
	fromInt 128 = InvalidInput
	fromInt 129 = InvalidName
	fromInt 130 = InvalidType
	fromInt 131 = ServerError
	fromInt 150 = ServerDown
	fromInt 151 = IllegalMethod
	fromInt 152 = IllegalRequest
	fromInt 153 = ServerTimeout
	fromInt 154 = DosProtection
	fromInt 155 = QueryTooLong
	fromInt i   = OtherCloogleError ("Unknown CloogleError " + toString i)

instance toString ABCArgument
where
	toString (ABCArgument type required)
	| required  = "[" <+ type <+ "]"
	| otherwise = toString type

instance fromString ABCArgument
where
	fromString s
	| 0 < size s && s.[0] == '[' && s.[size s-1] == ']'
		= ABCArgument (fromString (s % (1,size s-2))) True
		= ABCArgument (fromString s) True

instance toString ABCArgumentType
where
	toString t = case t of
		ABCTypeLabel        -> "label"
		ABCTypeAStackOffset -> "A-offset"
		ABCTypeBStackOffset -> "B-offset"
		ABCTypeAStackSize   -> "A-size"
		ABCTypeBStackSize   -> "B-size"
		ABCTypeBool         -> "bool"
		ABCTypeChar         -> "char"
		ABCTypeInt          -> "int"
		ABCTypeReal         -> "real"
		ABCTypeString       -> "string"

instance fromString ABCArgumentType
where
	fromString s = case s of
		"label"    -> ABCTypeLabel
		"A-offset" -> ABCTypeAStackOffset
		"B-offset" -> ABCTypeBStackOffset
		"A-size"   -> ABCTypeAStackSize
		"B-size"   -> ABCTypeBStackSize
		"bool"     -> ABCTypeBool
		"char"     -> ABCTypeChar
		"int"      -> ABCTypeInt
		"real"     -> ABCTypeReal
		"string"   -> ABCTypeString
		_          -> abort "failure in fromString of ABCArgumentType\n"

instance zero Request
where
	zero =
		{ unify            = ?None
		, name             = ?None
		, exactName        = ?None
		, className        = ?None
		, typeName         = ?None
		, using            = ?None
		, modules          = ?None
		, libraries        = ?None
		, include_builtins = ?None
		, include_core     = ?None
		, page             = ?None
		}

instance zero Response
where
	zero =
		{ return         = 0
		, msg            = "Success"
		, data           = []
		, more_available = ?None
		, suggestions    = ?None
		, warnings       = ?None
		}

instance toString Request where toString r = toString $ toJSON r
instance toString Response where toString r = toString (toJSON r) + "\n"

instance fromString (?Request) where fromString s = fromJSON $ fromString s

instance == FunctionKind where (==) a b = a === b

getBasicResult :: !Result -> BasicResult
getBasicResult (FunctionResult (br,_))       = br
getBasicResult (TypeResult (br,_))           = br
getBasicResult (ClassResult (br,_))          = br
getBasicResult (ModuleResult (br,_))         = br
getBasicResult (SyntaxResult (br,_))         = br
getBasicResult (ABCInstructionResult (br,_)) = br
getBasicResult (ProblemResult (br,_))        = br

parseSingleLineRequest :: !String -> MaybeErrorString Request
parseSingleLineRequest s = case split "::" s of
	["",type]   -> Ok {zero &                          unify= ?Just $ trim type}
	[name,type] -> Ok {zero & name= ?Just $ trim name, unify= ?Just $ trim type}
	[name]      -> Ok {Request | zero & name= ?Just $ trim name}
	_           -> Error "Multiple ::s found"

toSingleLine :: !Request -> ?String
toSingleLine req
| isJust req.typeName && isJust req.className = ?None
| isJust req.typeName  = req.typeName
| isJust req.className = req.className
| isJust req.unify     = case req.Request.name of
	?None   -> ?Just $ ":: " + fromJust req.unify
	?Just n -> ?Just $ n + " :: " + fromJust req.unify
| otherwise            = req.Request.name
