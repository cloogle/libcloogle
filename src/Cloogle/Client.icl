implementation module Cloogle.Client

/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of libcloogle.
 *
 * Libcloogle is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Libcloogle is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with libcloogle. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Control.Monad import class Monad(bind), =<<
import Data.Error
from Data.Func import $
import Data.Functor
import Data.Map
import Data.Maybe
import Data.Tuple
import Internet.HTTP
import Internet.HTTP.TCPIP
from StdFunc import flip, o
import StdOverloaded
import StdString
from Text import class Text(join), instance Text String, instance + String
import Text.Encodings.UrlEncoding
import Text.GenJSON

import Cloogle.API

toHTTPRequest :: !String !Request -> HTTPRequest
toHTTPRequest server req =
	{ newHTTPRequest
	& req_method   = HTTP_GET
	, req_path     = "/api.php"
	, req_query    = "?" + urlEncodePairs params
	, req_headers  = put "User-Agent" "Clean/libcloogle" newMap
	, server_name  = server
	}
where
	params = catMaybes
		[ tuple "str"                          <$> toSingleLine req
		, tuple "lib"              o join ","  <$> req.libraries
		, tuple "mod"              o join ","  <$> req.modules
		, tuple "include_builtins" o printBool <$> req.include_builtins
		, tuple "include_core"     o printBool <$> req.include_core
		, tuple "page"             o toString  <$> req.page
		]
	where printBool b = if b "true" "false"

request :: !String !Request !*World -> *(MaybeErrorString Response, *World)
request server req w = appFst ((=<<) parseResponse) $ doHTTPRequest (toHTTPRequest server req) 10000 w
where
	parseResponse :: HTTPResponse -> MaybeErrorString Response
	parseResponse res = case fromJSON $ fromString res.rsp_data of
		?None   -> Error "Invalid JSON"
		?Just r -> Ok r
