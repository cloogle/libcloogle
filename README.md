# libcloogle

This repository holds types for Cloogle, a search engine for [Clean][]. It is
used in the core system at
[cloogle/cloogle](https://gitlab.com/cloogle/cloogle). These types can be used
to easily access the TCP and HTTP APIs at a server running the web frontend
(source code at [cloogle/cloogle-web](https://gitlab.com/cloogle/cloogle-web)),
using the generic JSON functions on these types.

## Authors &amp; License

libcloogle is maintained by [Camil Staps][].

Copyright is owned by the authors of individual commits, including:

- Camil Staps
- Mart Lubbers

This project is licensed under AGPL v3 with additional terms under section 7;
see the [LICENSE](/LICENSE) file for details.

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
